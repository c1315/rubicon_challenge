package com.rubicon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WaterChallenge1Application {

	public static void main(String[] args) {
		SpringApplication.run(WaterChallenge1Application.class, args);
	}

}
