package com.rubicon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rubicon.entities.Orders;
import com.rubicon.service.OrderService;



@RestController
@RequestMapping("/orders")
public class OrdersController {
	
	@Autowired
	OrderService service;
	
	
//  TO to accept new orders from a farmer  :
// sample to pass in Postman app{
//     "farmId": 15,
//     "startDate": "2021-09-11",
//     "startTime": "22:16:20",
//     "duration": "00:01:02"
// }
	@PostMapping
	public ResponseEntity<?> save(@RequestBody Orders order) {		

		Orders result = service.acceptOrder(order);
		
		return ResponseEntity.ok(result);
	}
	
//  for cancelling an existing order if it hasn’t been delivered :	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> cancelOrder(@PathVariable("id") int farmId) {
		Boolean status = service.cancelOrder(farmId);
		return ResponseEntity.ok(status);
	}
	
// for querying an existing order :
   @GetMapping("/{farmId}")
   public ResponseEntity<String> queryOrder(@PathVariable("farmId") int farmId){
	   			String status = service.queryOrder(farmId);
	   			return ResponseEntity.ok(status);
   }
	
}
