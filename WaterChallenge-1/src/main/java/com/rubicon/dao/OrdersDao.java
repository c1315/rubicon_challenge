package com.rubicon.dao;



import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.rubicon.entities.Orders;

public interface OrdersDao extends JpaRepository<Orders, Integer> {
	
	List<Orders> findBystartDate(Date date);
	
	
//	 List<Orders> findAllBystartDateAndorderStatus(Date date , String orderStatus);
}
