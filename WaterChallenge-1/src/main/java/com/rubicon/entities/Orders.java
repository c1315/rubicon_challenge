package com.rubicon.entities;


import java.sql.Time;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ch.qos.logback.core.util.Duration;

@Entity
public class Orders {
	
	@Id
	private int farmId;
	@Temporal(TemporalType.DATE)
	private Date startDate;

	private Time startTime;
	
	private Time duration ;

	private Time endTime;
	private String orderStatus;
	
		
	public Orders() {
		super();
	}
	
	

	public int getFarmId() {
		return farmId;
	}



	public void setFarmId(int farmId) {
		this.farmId = farmId;
	}


	public Date getStartDate() {
		return startDate;
	}



	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}



	public Date getStartTime() {
		return startTime;
	}



	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}



	public Time getDuration() {
		return duration;
	}



	public void setDuration(Time duration) {
		this.duration = duration;
	}



	public Date getEndTime() {
		return endTime;
	}



	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}



	public String getOrderStatus() {
		return orderStatus;
	}



	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}



	@Override
	public String toString() {
		return "Orders [farmId=" + farmId + ", orderId=" + ", startDate=" + startDate + ", startTime="
				+ startTime + ", duration=" + duration + ", endTime=" + endTime + ", orderStatus=" + orderStatus + "]";
	}



}
