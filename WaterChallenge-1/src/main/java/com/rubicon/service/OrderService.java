package com.rubicon.service;

import java.util.Date;
import java.util.List;

import com.rubicon.entities.Orders;

public interface OrderService {
	
	Orders acceptOrder(Orders order);
	
	List<Orders> findBystartDate(Date date);
	
	Boolean cancelOrder(int farmId);
	
	String queryOrder(int farmId);
	
	void passOrder(Orders order);

	String getCornExpression(Orders order,String orderType);
	
//	 List<Orders> findAllBystartDateAndorderStatus(Date date , String orderStatus);
}
