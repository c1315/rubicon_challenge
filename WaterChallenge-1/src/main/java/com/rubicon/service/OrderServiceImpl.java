package com.rubicon.service;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.DefaultManagedTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import com.rubicon.dao.OrdersDao;
import com.rubicon.entities.Orders;
@Transactional
@Service
@EnableScheduling
public class OrderServiceImpl implements OrderService , SchedulingConfigurer {
	
	@Autowired
	private OrdersDao dao;
	String exp = "";
	String orderType = "";
	TaskScheduler schedular;
	List<String> cronList = new ArrayList<String>();
	
	@Override
	public List<Orders> findBystartDate(Date date) {
		return dao.findBystartDate(date);
	}
	
	
//   TO to accept new orders from a farmer  :
	
	@SuppressWarnings("deprecation")
	@Override
	public Orders acceptOrder(Orders order) {
	
		// calculating EndTime by adding duration to StartTime :
		Time endTime = new Time(0) ;
		
		int totalHours   = order.getDuration().getHours() + order.getStartTime().getHours(); 
		int totalMinutes = order.getDuration().getMinutes()+ order.getStartTime().getMinutes(); 
		int totalSeconds = order.getDuration().getSeconds()+ order.getStartTime().getSeconds(); 
		if (totalSeconds >= 60) { 
		  totalMinutes ++; 
		  totalSeconds = totalSeconds % 60; 
		} 
		if (totalMinutes >= 60) { 
		  totalHours ++; 
		  totalMinutes = totalMinutes % 60; 
		}  
		endTime.setHours(totalHours);
		endTime.setMinutes(totalMinutes);
		endTime.setSeconds(totalSeconds);
		
		order.setEndTime(endTime);
		
	// neglecting orders with status "Cancelled" and "delivered"while checking existing orders before saving new order

		List<Orders>list = dao.findBystartDate(order.getStartDate());
		List<Orders> filteredlist = new ArrayList<Orders>();
			for (Orders orderfromlist : list) {
				if( (  (orderfromlist.getOrderStatus().equals("Delivered")) || (orderfromlist.getOrderStatus().equals("cancelled"))) !=true) {
					filteredlist.add(orderfromlist);
				}
			}
		
		  if(filteredlist!=null) {
			  for (Orders orderFromDb : filteredlist) {
				  // for checking whether the orderTime is overlapping or not 
					if( (order.getEndTime().compareTo(orderFromDb.getStartTime()))<=0 && (order.getStartTime().compareTo(orderFromDb.getEndTime()))>=0 )
					{
						order.setOrderStatus("Requested");
						System.out.println("-----------------------------------------------------");
						System.out.println("Order has been placed but not yet delivered");
						System.out.println("-----------------------------------------------------");
						dao.save(order);
						
						  orderType = "InProgress" ;
						  exp = getCornExpression(order,orderType);
						  passOrder(order);
						  cronList.add(exp);
						  
						  orderType = "Delivered" ;
						  exp = getCornExpression(order,orderType);
					      return order;
						
					}
					else {
						System.out.println("-----------------------------------------------------");
						System.out.println("Order time is Overlapping!");
						System.out.println("-----------------------------------------------------");
						return null;
					}
				}}
		  order.setOrderStatus("Requested");
		  System.out.println("-----------------------------------------------------");
		  System.out.println("Order has been placed but not yet delivered");
		  System.out.println("-----------------------------------------------------");
		  dao.save(order);
			  orderType = "InProgress" ;
			  exp = getCornExpression(order,orderType); 
			  passOrder(order);
			  cronList.add(exp);
				
			  orderType = "Delivered" ;
			  exp = getCornExpression(order,orderType);
			  cronList.add(exp);
  		  	  return order;
	 }
	
	
//  for cancelling an existing order if it hasn’t been delivered :
	@SuppressWarnings("deprecation")
	@Override
	public Boolean cancelOrder(int farmId) {
		   
		if(dao.existsById(farmId)){
			  Orders result = dao.getById(farmId);
			  if((result.getOrderStatus()).equals("Requested")) {
				  String status = "cancelled";
				  result.setOrderStatus(status);
				  dao.save(result);
				  System.out.println("-----------------------------------------------------");
				  System.out.println("order with farmId :"+result.getFarmId()+ "  has been cancelled");
				  System.out.println("-----------------------------------------------------");
				  return true;  
			  }
			}
		return false;
	}


	@Override
	public String queryOrder(int farmId) {
				Optional<Orders> result = dao.findById(farmId);
				if (result.isPresent()) {
					return result.get().getOrderStatus();
				}
				return "order does not exist";
	}
	
// to accept order so can be passed to scheduler; 
	Orders orderToBeChanged ;
 
	@Override
	public void passOrder(Orders order) {
			orderToBeChanged = order;
	}		
	
	@Override
	public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
		String initial = "0 0/1 * * 12 *";
		  cronList.add(initial);
		  System.out.println("inside schedular");
	 for (String cornExpression : cronList) {
		
		 Runnable runnable = new Runnable() { 
				
				@Override
				public void run() {
					if(orderToBeChanged.getOrderStatus().equals("Requested")) {
						orderToBeChanged.setOrderStatus("InProgress");
						dao.save(orderToBeChanged);
						System.out.println("----------------------------------------------------");
						System.out.println("Order is InProgress"+" from "+orderToBeChanged.getStartTime()+" to "+orderToBeChanged.getEndTime() );
						System.out.println("-----------------------------------------------------");
					}
					else if (orderToBeChanged.getOrderStatus().equals("Requested")) {
						orderToBeChanged.setOrderStatus("Delivered");
						dao.save(orderToBeChanged);
						System.out.println("----------------------------------------------------");
						System.out.println("Order is Delivered"+" from "+orderToBeChanged.getStartTime()+" to "+orderToBeChanged.getEndTime() );
						System.out.println("-----------------------------------------------------");
					
					}
				}
			};
			
	           Trigger trigger = new Trigger() {
	        	@Override
	            public Date nextExecutionTime(TriggerContext triggerContext) {
	        		CronTrigger crontrigger = new CronTrigger(cornExpression);
	        		return crontrigger.nextExecutionTime(triggerContext);
	            }
	        };
	        scheduledTaskRegistrar.addTriggerTask(runnable, trigger);
	 }   
	}

	
// for forming cornExpression from a given order's startDate and time : 
	@SuppressWarnings("deprecation")
	@Override
	public String getCornExpression(Orders order, String orderType) {
		
		
		if(orderType.equals("InProgress")) {
		
			System.out.println(order.getStartDate());
			int hrs = order.getStartTime().getHours();
		int min = order.getStartTime().getMinutes();
		int sec = order.getStartTime().getSeconds();
		
		int day = order.getStartDate().getDate();
		int month = order.getStartDate().getMonth()+1;
		int year = order.getStartDate().getYear()+1900;
		
		String expression = String.format("%d %d %d %d %d ? %d ",sec,min,hrs,day,month,year);
		return expression;
	   }
		else {
			
			int hrs = order.getEndTime().getHours();
			int min = order.getEndTime().getMinutes();
			int sec = order.getEndTime().getSeconds();
			
			int day = order.getStartDate().getDate();
			int month = order.getStartDate().getMonth()+1;
			int year = order.getStartDate().getYear()+1900;
			String expression = String.format("%d %d %d %d %d ? %d ",sec,min,hrs,day,month,year);
			return expression ;
		}
	}

	
}

